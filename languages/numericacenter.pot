#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: DKConduite\n"
"POT-Creation-Date: 2021-06-08 02:08+0100\n"
"PO-Revision-Date: 2021-06-08 01:45+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.3\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-Flags-xgettext: --add-comments=translators:\n"
"X-Poedit-WPHeader: style.css\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;"
"_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.min.js\n"

#: 404.php:17
msgid "Not Found"
msgstr ""

#: 404.php:22
msgid "Try searching or "
msgstr ""

#: 404.php:22
msgid "Go back to Home"
msgstr ""

#: archive.php:29
#, php-format
msgid "Daily Archives: %s"
msgstr ""

#: archive.php:31
#, php-format
msgid "Monthly Archives: %s"
msgstr ""

#: archive.php:31
msgctxt "monthly archives date format"
msgid "F Y"
msgstr ""

#: archive.php:33
#, php-format
msgid "Yearly Archives: %s"
msgstr ""

#: archive.php:33
msgctxt "yearly archives date format"
msgid "Y"
msgstr ""

#: archive.php:35
msgid "Archives"
msgstr ""

#: category.php:19
#, php-format
msgid "Category Archives: %s"
msgstr ""

#: comments.php:24
#, php-format
msgctxt "comments title"
msgid "One thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] ""
msgstr[1] ""

#: comments.php:44
msgid "Comment navigation"
msgstr ""

#: comments.php:45
msgid "&larr; Older Comments"
msgstr ""

#: comments.php:46
msgid "Newer Comments &rarr;"
msgstr ""

#: comments.php:51
msgid "Comments are closed."
msgstr ""

#: content-none.php:11
msgid "Nothing Found"
msgstr ""

#: content-none.php:17
#, php-format
msgid ""
"Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: content-none.php:21
msgid ""
"Sorry, but nothing matched your search terms. Please try again with "
"different keywords."
msgstr ""

#: content-none.php:26
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps "
"searching can help."
msgstr ""

#: content.php:30 image.php:43 page.php:38
msgid "Edit"
msgstr ""

#: content.php:40
msgid "Continue reading <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: content.php:41 image.php:69 page.php:34
msgid "Pages:"
msgstr ""

#: content.php:48
msgid "Leave a comment"
msgstr ""

#: content.php:48
msgid "One comment so far"
msgstr ""

#: content.php:48
#, php-format
msgid "View all % comments"
msgstr ""

#: functions.php:85
msgid "Navigation Menu"
msgstr ""

#: functions.php:116
msgctxt "Source Sans Pro font: on or off"
msgid "on"
msgstr ""

#: functions.php:122
msgctxt "Bitter font: on or off"
msgid "on"
msgstr ""

#: functions.php:219
#, php-format
msgid "Page %s"
msgstr ""

#: functions.php:232
msgid "Main Widget Area"
msgstr ""

#: functions.php:234
msgid "Appears in the footer section of the site."
msgstr ""

#: functions.php:242
msgid "Secondary Widget Area"
msgstr ""

#: functions.php:244
msgid "Appears on posts and pages in the sidebar."
msgstr ""

#: functions.php:267
msgid "Posts navigation"
msgstr ""

#: functions.php:271
msgid "<span class=\"meta-nav\">&larr;</span> Older posts"
msgstr ""

#: functions.php:275
msgid "Newer posts <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: functions.php:301
msgid "Post navigation"
msgstr ""

#: functions.php:304
msgid "Previous Article"
msgstr ""

#: functions.php:308
msgid "Next Article"
msgstr ""

#: functions.php:326
msgid "Sticky"
msgstr ""

#: functions.php:332 functions.php:338
msgid ", "
msgstr ""

#: functions.php:347
#, php-format
msgid "View all posts by %s"
msgstr ""

#: functions.php:367
#, php-format
msgctxt "1: post format name. 2: date"
msgid "%1$s on %2$s"
msgstr ""

#: functions.php:373
#, php-format
msgid "Permalink to %s"
msgstr ""

#: functions.php:546
msgctxt "post type general name"
msgid "Members"
msgstr ""

#: functions.php:547
msgctxt "post type singular name"
msgid "Member"
msgstr ""

#: functions.php:548
msgctxt "admin menu"
msgid "Members"
msgstr ""

#: functions.php:549
msgctxt "add new on admin bar"
msgid "Member"
msgstr ""

#: functions.php:550
msgctxt "member"
msgid "Add New"
msgstr ""

#: functions.php:551
msgid "Add New Member"
msgstr ""

#: functions.php:552
msgid "New Member"
msgstr ""

#: functions.php:553
msgid "Edit Member"
msgstr ""

#: functions.php:554
msgid "View Member"
msgstr ""

#: functions.php:555
msgid "All Members"
msgstr ""

#: functions.php:556
msgid "Search Members"
msgstr ""

#: functions.php:557
msgid "Parent Members:"
msgstr ""

#: functions.php:558
msgid "No members found."
msgstr ""

#: functions.php:559
msgid "No members found in Trash."
msgstr ""

#: functions.php:564
msgid "Description."
msgstr ""

#: image.php:21
#, php-format
msgid ""
"<span class=\"attachment-meta\">Published on <time class=\"entry-date\" "
"datetime=\"%1$s\">%2$s</time> in <a href=\"%3$s\" title=\"Return to %4$s\" "
"rel=\"gallery\">%5$s</a></span>"
msgstr ""

#: image.php:37
msgid "Link to full-size image"
msgstr ""

#: image.php:38
msgid "Full resolution"
msgstr ""

#: image.php:50
msgid "<span class=\"meta-nav\">&larr;</span> Previous"
msgstr ""

#: image.php:51
msgid "Next <span class=\"meta-nav\">&rarr;</span>"
msgstr ""

#: search.php:17
#, php-format
msgid "Search Results for: %s"
msgstr ""

#: tag.php:20
#, php-format
msgid "Tag Archives: %s"
msgstr ""

#. Theme Name of the plugin/theme
msgid "DKConduite"
msgstr ""

#. Theme URI of the plugin/theme
msgid "http://dkconduite.com"
msgstr ""

#. Description of the plugin/theme
msgid ""
"DKConduite official WordPress of dkconduite.com. A theme based on "
"Bootship, a startup theme for Advanced user who want to build professional "
"WordPress theme based on Bootstrap"
msgstr ""

#. Author of the plugin/theme
msgid "FGKNUMERIC"
msgstr ""

#. Author URI of the plugin/theme
msgid ""
msgstr ""
