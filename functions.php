<?php
/**
 * felix functions and definitions
 *
 * Sets up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters, @link http://codex.wordpress.org/Plugin_API
 *
 * @package WordPress
 * @since Felix
 */

/*
 * Set up the content width value based on the theme's design.
 *
 * @see felix_content_width() for template-specific adjustments.
 */
if ( ! isset( $content_width ) )
	$content_width = 730;

/**
 * felix setup. 
 *
 * Sets up theme defaults and registers the various WordPress features that
 * felix supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add Visual Editor stylesheets.
 * @uses add_theme_support() To add support for automatic feed links, post
 * formats, and post thumbnails.
 * @uses register_nav_menu() To add support for a navigation menu.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since Felix
 */
function felix_setup() {
	/*
	 * Makes felix available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on felix, use a find and
	 * replace to change 'felix' to the name of your theme in all
	 * template files.
	 */
	load_theme_textdomain( 'felix', get_template_directory() . '/languages' );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', 'https://use.fontawesome.com/releases/v5.0.13/css/all.css', felix_fonts_url() ) );

	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );
  
  // Adds support for WooCommerce customization
  add_theme_support( 'woocommerce' );

	/*
	 * Switches default core markup for search form, comment form,
	 * and comments to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * This theme supports all available post formats by default.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'
	) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', __( 'Navigation Menu', 'felix' ) );

	/*
	 * This theme uses a custom image size for featured images, displayed on
	 * "standard" posts and pages.
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 728, 300, true );

	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );
}
add_action( 'after_setup_theme', 'felix_setup' );

/**
 * Return the Google font stylesheet URL, if available.
 *
 * The use of Source Sans Pro and Bitter by default is localized. For languages
 * that use characters not supported by the font, the font can be disabled.
 *
 * @since Felix
 *
 * @return string Font stylesheet or empty string if disabled.
 */
function felix_fonts_url() {
	$fonts_url = '';

	/* Translators: If there are characters in your language that are not
	 * supported by Source Sans Pro, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$source_sans_pro = _x( 'on', 'Source Sans Pro font: on or off', 'felix' );

	/* Translators: If there are characters in your language that are not
	 * supported by Bitter, translate this to 'off'. Do not translate into your
	 * own language.
	 */
	$bitter = _x( 'on', 'Bitter font: on or off', 'felix' );

	if ( 'off' !== $source_sans_pro || 'off' !== $bitter ) {
		$font_families = array();

		if ( 'off' !== $source_sans_pro ) 
			$font_families[] = 'Open Sans:300;0,400;0,600;0,700;1,400;1,500';

		if ( 'off' !== $bitter )
			$font_families[] = 'Bitter:400,700';

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);
		$fonts_url = add_query_arg( $query_args, "//fonts.googleapis.com/css" );
	}

	return $fonts_url;
}

/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Felix
 */
function felix_scripts_styles() {
  /*
   * Adds JavaScript to pages with the comment form to support
   * sites with threaded comments (when in use).
   */
  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
    wp_enqueue_script( 'comment-reply' );

  // Adds Masonry to handle vertical alignment of footer widgets.
  if ( is_active_sidebar( 'sidebar-1' ) )
    wp_enqueue_script( 'jquery-masonry' );

  // Loads JavaScript file with functionality specific to Boot Ship.
  wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.js', array( 'jquery', 'popper' ), '5.1.3', true );
  wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery', 'popper' ), '5.1.3', true );
  wp_enqueue_script( 'felix-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery', 'wow', 'slick' ), '2020-08-09', true );
  wp_enqueue_script( 'popper', get_template_directory_uri() . '/js/popper.js', array(), '1.16.0', true );
 

  // WOW.js @link https://github.com/matthieua/WOW
  wp_enqueue_script( 'wow', get_template_directory_uri() . '/js/wow.js', array( 'jquery' ), '1.1.3', true );
  
  // Slick.js @link https://kenwheeler.github.io/slick/
  wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.js', array( 'jquery' ), '1.8.0', true );

	// aos.js @link https://michalsnik.github.io/aos/
  wp_enqueue_script( 'aos', get_template_directory_uri() . '/js/aos.js', array( 'jquery' ), '', true );

	// Typed.js @link https://github.com/mattboldt/typed.js
  wp_enqueue_script( 'typed', get_template_directory_uri() . '/js/typed.min.js', array( 'jquery' ), '2.0.11', true );

  // Add Source Sans Pro and Bitter fonts, used in the main stylesheet.
  wp_enqueue_style( 'felix-fonts', felix_fonts_url(), array(), null );

  // Loads our main stylesheet.
  wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), '5.1.3' );
  wp_enqueue_style( 'bootstrap-icons', get_template_directory_uri() . '/css/bootstrap-icons.css', array(), '1.5.0' );
  wp_enqueue_style( 'felix-theme', get_template_directory_uri() . '/css/theme.css', array(), '1.0' );
  wp_enqueue_style( 'felix-style', get_stylesheet_uri(), array(), '2022-04-15' );

  // Font Awesome stylesheet
  wp_enqueue_style( 'fontawesome', 'https://use.fontawesome.com/releases/v5.15.1/css/all.css', array(), '5.15.1' );

  // Animate CSS @link: https://animate.style stylesheet
  wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.css', array(), '4.1.1' );

  // Hover CSS @link: https://github.com/IanLunn/Hover stylesheet
  wp_enqueue_style( 'hover', get_template_directory_uri() . '/css/hover.css', array(), '2.3.2' );

	// Oas @link: https://michalsnik.github.io/aos/
  wp_enqueue_style( 'aos', get_template_directory_uri() . '/css/aos.css', array(), '' );
  
  // Slick.css @link https://kenwheeler.github.io/slick/
  wp_enqueue_style( 'slick', get_template_directory_uri() . '/css/slick.css', array(), '' );
  wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/css/slick-theme.css', array(), '' );

  // Loads the Internet Explorer specific stylesheet.
  wp_enqueue_style( 'felix-ie', get_template_directory_uri() . '/css/ie.css', array( 'felix-style' ), '2016-08-09' );
  wp_style_add_data( 'felix-ie', 'conditional', 'lt IE 9' );
}
add_action( 'wp_enqueue_scripts', 'felix_scripts_styles');

/**
 * Filter the page title.
 *
 * Creates a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Felix
 *
 * @param string $title Default title text for current view.
 * @param string $sep   Optional separator.
 * @return string The filtered title.
 */
function felix_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'felix' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'felix_wp_title', 10, 2 );

/**
 * Register two widget areas.
 *
 * @since Felix
 */
function felix_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Main Widget Area', 'felix' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Appears in the footer section of the site.', 'felix' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Secondary Widget Area', 'felix' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Appears on posts and pages in the sidebar.', 'felix' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'felix_widgets_init' );

if ( ! function_exists( 'felix_paging_nav' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 *
 * @since Felix
 */
function felix_paging_nav() {
	global $wp_query;

	// Don't print empty markup if there's only one page.
	if ( $wp_query->max_num_pages < 2 )
		return;
	?>
	<nav class="navigation paging-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'felix' ); ?></h1>
		<div class="nav-links">

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'felix' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'felix' ) ); ?></div>
			<?php endif; ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'felix_post_nav' ) ) :
/**
 * Display navigation to next/previous post when applicable.
*
* @since Felix
*/
function felix_post_nav() {
	global $post;

	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous )
		return;
	?>
	<nav class="navigation post-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Post navigation', 'felix' ); ?></h1>
		<div class="nav-links">
			<div class="nav-previous">
				<?php previous_post_link( '<span class="nav-links__label">' . esc_html__( 'Previous Article', 'felix' ) . '</span> %link' ); ?>
			</div>
			
			<div class="nav-next">
				<?php next_post_link( '<span class="nav-links__label">' . esc_html__( 'Next Article', 'felix' ) . '</span> %link' ); ?>
			</div>
		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'felix_entry_meta' ) ) :
/**
 * Print HTML with meta information for current post: categories, tags, permalink, author, and date.
 *
 * Create your own felix_entry_meta() to override in a child theme.
 *
 * @since Felix
 */
function felix_entry_meta() {
	if ( is_sticky() && is_home() && ! is_paged() )
		echo '<span class="featured-post">' . __( 'Sticky', 'felix' ) . '</span>';

	if ( ! has_post_format( 'link' ) && 'post' == get_post_type() )
		felix_entry_date();

	// Translators: used between list items, there is a space after the comma.
	$categories_list = get_the_category_list( __( ', ', 'felix' ) );
	if ( $categories_list ) {
		echo '<span class="categories-links">' . $categories_list . '</span>';
	}

	// Translators: used between list items, there is a space after the comma.
	$tag_list = get_the_tag_list( '', __( ', ', 'felix' ) );
	if ( $tag_list ) {
		echo '<span class="tags-links">' . $tag_list . '</span>';
	}

	// Post author
	if ( 'post' == get_post_type() ) {
		printf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			esc_attr( sprintf( __( 'View all posts by %s', 'felix' ), get_the_author() ) ),
			get_the_author()
		);
	}
}
endif;

if ( ! function_exists( 'felix_entry_date' ) ) :
/**
 * Print HTML with date information for current post.
 *
 * Create your own felix_entry_date() to override in a child theme.
 *
 * @since Felix
 *
 * @param boolean $echo (optional) Whether to echo the date. Default true.
 * @return string The HTML-formatted post date.
 */
function felix_entry_date( $echo = true ) {
	if ( has_post_format( array( 'chat', 'status' ) ) )
		$format_prefix = _x( '%1$s on %2$s', '1: post format name. 2: date', 'felix' );
	else
		$format_prefix = '%2$s';

	$date = sprintf( '<span class="date"><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',
		esc_url( get_permalink() ),
		esc_attr( sprintf( __( 'Permalink to %s', 'felix' ), the_title_attribute( 'echo=0' ) ) ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( sprintf( $format_prefix, get_post_format_string( get_post_format() ), get_the_date() ) )
	);

	if ( $echo )
		echo $date;

	return $date;
}
endif;

if ( ! function_exists( 'felix_the_attached_image' ) ) :
/**
 * Print the attached image with a link to the next attached image.
 *
 * @since Felix
 */
function felix_the_attached_image() {
	/**
	 * Filter the image attachment size to use.
	 *
	 * @since Felix
	 *
	 * @param array $size {
	 *     @type int The attachment height in pixels.
	 *     @type int The attachment width in pixels.
	 * }
	 */
	$attachment_size     = apply_filters( 'felix_attachment_size', array( 724, 724 ) );
	$next_attachment_url = wp_get_attachment_url();
	$post                = get_post();

	/*
	 * Grab the IDs of all the image attachments in a gallery so we can get the URL
	 * of the next adjacent image in a gallery, or the first image (if we're
	 * looking at the last image in a gallery), or, in a gallery of one, just the
	 * link to that image file.
	 */
	$attachment_ids = get_posts( array(
		'post_parent'    => $post->post_parent,
		'fields'         => 'ids',
		'numberposts'    => -1,
		'post_status'    => 'inherit',
		'post_type'      => 'attachment',
		'post_mime_type' => 'image',
		'order'          => 'ASC',
		'orderby'        => 'menu_order ID'
	) );

	// If there is more than 1 attachment in a gallery...
	if ( count( $attachment_ids ) > 1 ) {
		foreach ( $attachment_ids as $attachment_id ) {
			if ( $attachment_id == $post->ID ) {
				$next_id = current( $attachment_ids );
				break;
			}
		}

		// get the URL of the next image attachment...
		if ( $next_id )
			$next_attachment_url = get_attachment_link( $next_id );

		// or get the URL of the first image attachment.
		else
			$next_attachment_url = get_attachment_link( array_shift( $attachment_ids ) );
	}

	printf( '<a href="%1$s" title="%2$s" rel="attachment">%3$s</a>',
		esc_url( $next_attachment_url ),
		the_title_attribute( array( 'echo' => false ) ),
		wp_get_attachment_image( $post->ID, $attachment_size )
	);
}
endif;

/**
 * Return the post URL.
 *
 * @uses get_url_in_content() to get the URL in the post meta (if it exists) or
 * the first link found in the post content.
 *
 * Falls back to the post permalink if no URL is found in the post.
 *
 * @since Felix
 *
 * @return string The Link format URL.
 */
function felix_get_link_url() {
	$content = get_the_content();
	$has_url = get_url_in_content( $content );

	return ( $has_url ) ? $has_url : apply_filters( 'the_permalink', get_permalink() );
}

/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Active widgets in the sidebar to change the layout and spacing.
 * 3. When avatars are disabled in discussion settings.
 *
 * @since Felix
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function felix_body_class( $classes ) {
	if ( ! is_multi_author() )
		$classes[] = 'single-author';

	if ( is_active_sidebar( 'sidebar-2' ) && ! is_attachment() && ! is_404() )
		$classes[] = 'sidebar';

	if ( ! get_option( 'show_avatars' ) )
		$classes[] = 'no-avatars';

	return $classes;
}
add_filter( 'body_class', 'felix_body_class' );

/**
 * Adjust content_width value for video post formats and attachment templates.
 *
 * @since Felix
 */
function felix_content_width() {
	global $content_width;

	if ( is_attachment() )
		$content_width = 724;
	elseif ( has_post_format( 'audio' ) )
		$content_width = 484;
}
add_action( 'template_redirect', 'felix_content_width' );

/**
 * Add postMessage support for site title and description for the Customizer.
 *
 * @since Felix
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
function felix_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'felix_customize_register' );

/**
 * Enqueue Javascript postMessage handlers for the Customizer.
 *
 * Binds JavaScript handlers to make the Customizer preview
 * reload changes asynchronously.
 *
 * @since Felix
 */
function felix_customize_preview_js() {
	wp_enqueue_script( 'felix-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array( 'customize-preview' ), '20130226', true );
}
add_action( 'customize_preview_init', 'felix_customize_preview_js' );




/**
 * Register Portfolios posts types.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 *
 * @since Felix
 */
function felix_register_post_type() {
	$labels = array(
		'name'               => _x( 'portfolios', 'post type general name', 'felix' ),
		'singular_name'      => _x( 'portfolio', 'post type singular name', 'felix' ),
		'menu_name'          => _x( 'portfolios', 'admin menu', 'felix' ),
		'name_admin_bar'     => _x( 'portfolio', 'add new on admin bar', 'felix' ),
		'add_new'            => _x( 'Add New', 'portfolio', 'felix' ),
		'add_new_item'       => __( 'Add New portfolio', 'felix' ),
		'new_item'           => __( 'New portfolio', 'felix' ),
		'edit_item'          => __( 'Edit portfolio', 'felix' ),
		'view_item'          => __( 'View portfolio', 'felix' ),
		'all_items'          => __( 'All portfolios', 'felix' ),
		'search_items'       => __( 'Search portfolios', 'felix' ),
		'parent_item_colon'  => __( 'Parent portfolios:', 'felix' ),
		'not_found'          => __( 'No portfolios found.', 'felix' ),
		'not_found_in_trash' => __( 'No portfolios found in Trash.', 'felix' )
	);

	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Description.', 'felix' ),
		'public'             => true,
		'publicly_queryable' => true,
		'taxonomies'		 => array( 'categories', 'felix' ),
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'portfolio' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);
 
	register_post_type( 'portfolio', $args );
}
add_action( 'init', 'felix_register_post_type' );






//hook into the init action and call create_topics_nonhierarchical_taxonomy when it fires
 
add_action( 'init', 'create_topics_nonhierarchical_taxonomy', 0 );
 
function create_topics_nonhierarchical_taxonomy() {
 
// Labels part for the GUI
 
  $labels = array(
    'name' => _x( 'Category', 'taxonomy general name' ),
    'singular_name' => _x( 'Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Topics' ),
    'popular_items' => __( 'Popular Categories' ),
    'all_items' => __( 'All Category' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Category' ), 
    'update_item' => __( 'Update Category' ),
    'add_new_item' => __( 'Add New Category' ),
    'new_item_name' => __( 'New Category Name' ),
    'separate_items_with_commas' => __( 'Separate Categories with commas' ),
    'add_or_remove_items' => __( 'Add or remove Categories' ),
    'choose_from_most_used' => __( 'Choose from the most used Categories' ),
    'menu_name' => __( 'Categories' ),
  ); 
 
// Now register the non-hierarchical taxonomy like tag
 
  register_taxonomy('topics','portfolio',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_in_rest' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'topic' ),
  ));
}





/**
 * Add meta boxes to the post edit screen.
 *
 * @since Felix
 */
function felix_add_meta_boxes() {
	add_meta_box( 'portfolio_details', __( 'Contactor', 'felix' ), 'portfolio_details', 'portfolio', 'normal', 'high' );
}
add_action( 'add_meta_boxes', 'felix_add_meta_boxes' );

/**
 * Output meta box Petition details.
 *
 * @since Felix
 */
function portfolio_details( $post ) {
	$portfolio_contractor_name = get_post_meta( $post->ID, '_portfolio_contractor_name', true );

	echo '<p>';
		echo '<label for="portfolio_contractor_name">' . __('Name:', 'felix' ) . '</label> ';
		echo '<input id="portfolio_contractor_name" name="portfolio_contractor_name" type="text" style="width:99%;" value="' . $portfolio_contractor_name . '" />';
	echo '</p>';
}

/**
 * Save content posted in custom meta boxes.
 *
 * @since Felix
 */
function felix_save_post( $post_id, $post ) {
	if ( ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) || defined('DOING_AJAX') )
		return $post_id;

	if( 'portfolio' == $post->post_type ) {
		if ( !current_user_can( 'edit_post', $post_id ) )
			return $post_id;

		if ( isset( $_POST['portfolio_contractor_name'] ) )
			update_post_meta( $post_id, '_portfolio_contractor_name', $_POST['portfolio_contractor_name'] );
	}
	return $post_id;
}
add_action( 'save_post', 'felix_save_post', 10, 2 );



/**
 * Add ACF Option Page
 *
 * @since soaga 1.0
 */

if ( function_exists ('acf_add_options_page') ) {
	$parent = acf_add_options_page(array(
	  'page_title' => __('FELIX Options', 'felix'),
	  'menu_title' 	=> __('FELIX', 'felix'),
	  'capability' => 'manage_options',
	  'position' => '1',
	  'icon_url' => 'dashicons-layout',
	  'post_id' => 'akka_options',
	  'update_button'		=> __('Update', 'felix'),
	  'updated_message'	=> __("Options Updated", 'felix'),
	));
  
	// add Theme Options page
	acf_add_options_sub_page(array(
	  'page_title' 	=> __('Theme Options', 'felix'),
	  'menu_title' 	=> __('Theme', 'felix'),
	  'parent_slug' 	=> $parent['menu_slug'],
	));
  
	// add Settings page
	acf_add_options_sub_page(array(
	  'page_title' 	=> __('Settings Page', 'felix'),
	  'menu_title' 	=> __('Settings', 'felix'),
	  'parent_slug' 	=> $parent['menu_slug'],
	));
  }
  


	
  
  